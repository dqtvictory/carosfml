#include "Controller.h"
#include <chrono>
#include <thread>
#include <iostream>

static inline int myMin(int x, int y)
{
	return std::min(x, y);
}

static inline int myMax(int x, int y)
{
	return std::max(x, y);
}

static std::pair<int, int> alphaBeta(Gomoku game, int depth, int alpha, int beta, int currentMove)
{
	if (!depth || game.IsEnd())
		return { currentMove, game.Eval() };
	auto comp{ (game.CurrentPlayer == PLAYER_X) ? myMax : myMin };
	int val{ (game.CurrentPlayer == PLAYER_X) ? alpha : beta };
	int bestMove{ -1 };

	vector<int> moveQueue;
	moveQueue.reserve(BOARD_SIZE);
	game.GetPossibleMoves(moveQueue);
	for (auto& moveIdx : moveQueue)
	{
		if (alpha >= beta)
			break;
		game.Move(moveIdx);
		auto tmp = comp(val, alphaBeta(game, depth - 1, alpha, beta, moveIdx).second);
		game.UndoMove(moveIdx);
		if (tmp != val)
		{
			val = tmp;
			bestMove = moveIdx;
			if (game.CurrentPlayer == PLAYER_X)
				alpha = val;
			else
				beta = val;
		}
	}
	return { bestMove, val };
}

Controller::Controller(int humanPlayer, int cpuLevel) :
	human{ humanPlayer },
	cpuDepth{ cpuLevel },
	humanMoveIdx{ -1 },
	cpuMoveIdx{ -1 },
	gui{ PLAYER_X, PLAYER_X, humanMoveIdx, cpuMoveIdx }
{}

void Controller::MainLoop()
{
	std::thread t{ &Controller::loop, this };
	gui.GUILoop();
	t.join();
}

void Controller::loop()
{
	while (true)
	{
		if (game.IsEnd())
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			if (game.Winner)
				gui.HasWinner(game.Winner);
			break;
		}
		if (game.CurrentPlayer != human)
		{
			cpuMoveIdx = cpuPlay();
			game.Move(cpuMoveIdx);
			humanMoveIdx = -1;
		}
		else if (humanMoveIdx != -1)
			game.Move(humanMoveIdx);
	}
}

int Controller::cpuPlay()
{
	return alphaBeta(game, cpuDepth, INT32_MIN, INT32_MAX, -1).first;
}
