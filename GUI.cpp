#include "GUI.h"
#include <iostream>

constexpr unsigned int gridDim{ (SQUARE_MARGIN + SQUARE_DIM) * BOARD_DIM + SQUARE_MARGIN };

static inline int rcToIdx(int const& r, int const& c)
{
	return r * BOARD_DIM + c;
}

static inline std::pair<int, int> idxToRC(int const& idx)
{
	return { idx / BOARD_DIM, idx % BOARD_DIM };
}

GUI::GUI(int firstPlayer, int humanPlayer, int& humanMoveIdx, int& cpuMoveIdx):
	needRedraw{ true },
	window{
		sf::VideoMode{ gridDim, gridDim + TEXT_SIZE + 5 * SQUARE_MARGIN },
		WIN_TITLE,
		sf::Style::Titlebar | sf::Style::Close
	},
	human{ humanPlayer },
	cpu{ -humanPlayer },
	highlightIdx{ -1 },
	lastMoveIdx{ -1 },
	current{ firstPlayer },
	winner{ 0 },
	humanMoveIdx{ humanMoveIdx },
	cpuMoveIdx{ cpuMoveIdx }
{

	window.setFramerateLimit(24);
	if (!font.loadFromFile(FONT_FNAME))
		exit(1);
	status.setFont(font);
	status.setCharacterSize(TEXT_SIZE);

	plays[PLAYER_X];
	plays[PLAYER_O];
	for (int r = 0; r < BOARD_DIM; ++r)
		for (int c = 0; c < BOARD_DIM; ++c)
		{
			int i{ r * BOARD_DIM + c };
			sf::RectangleShape& rect{ grid[i] };
			rect.setSize({ SQUARE_DIM, SQUARE_DIM });
			rect.setFillColor(squareNormal);
			rect.setPosition(
				1.f * SQUARE_MARGIN + c * (SQUARE_DIM + SQUARE_MARGIN),
				1.f * SQUARE_MARGIN + r * (SQUARE_DIM + SQUARE_MARGIN)
			);
		}
}

void GUI::GUILoop()
{
	while (window.isOpen())
	{
		if (needRedraw)
			drawWindow();
		if (window.pollEvent(ev))
		{
			switch (ev.type)
			{
			case sf::Event::Closed:
				window.close();
				exit(0);
			case sf::Event::MouseMoved:
				handleMouseMove(ev.mouseMove.x, ev.mouseMove.y);
				break;
			case sf::Event::MouseButtonPressed:	
				handleMouseClick(ev.mouseMove.x, ev.mouseMove.y);
				break;
			case sf::Event::MouseLeft:
				handleMouseLeftWindow();
				break;
			default:
				break;
			}
		}
		if (cpuMoveIdx != -1)
		{
			playerMove(cpu, cpuMoveIdx);
			cpuMoveIdx = -1;
		}
	}
}

void GUI::HasWinner(int player)
{
	winner = player;
	current = 0;
	needRedraw = true;
}

bool GUI::gameEnded()
{
	return (winner != 0 || (plays[PLAYER_X].size() + plays[PLAYER_O].size() == BOARD_SIZE));
}

void GUI::drawWindow()
{
	window.clear(windowBG);
	for (int i = 0; i < BOARD_SIZE; ++i)
		window.draw(grid[i]);
	for (auto& i : plays.at(PLAYER_X))
		drawX(grid[i].getPosition());
	for (auto& i : plays.at(PLAYER_O))
		drawO(grid[i].getPosition());
	drawStatus();
	window.display();
	needRedraw = false;
}

void GUI::playerMove(int player, int moveIdx)
{
	if (player != current)
		return;
	plays[player].insert(moveIdx);
	if (lastMoveIdx != -1)
		grid[lastMoveIdx].setFillColor(squareNormal);
	grid[moveIdx].setFillColor(squareMoveHighlight);
	lastMoveIdx = moveIdx;
	current *= -1;
	needRedraw = true;
}

void GUI::handleMouseMove(int const& mouseX, int const& mouseY)
{
	if (gameEnded())
		return;
	if (mouseX < gridDim && mouseY < gridDim)
	{
		int r{ (mouseY - SQUARE_MARGIN) / (SQUARE_DIM + SQUARE_MARGIN) };
		int c{ (mouseX - SQUARE_MARGIN) / (SQUARE_DIM + SQUARE_MARGIN) };
		int idx{ rcToIdx(r, c) };
		if (plays[PLAYER_X].find(idx) != plays[PLAYER_X].end()
			|| plays[PLAYER_O].find(idx) != plays[PLAYER_O].end())
			idx = -1;
		if (idx != highlightIdx)
		{
			if (highlightIdx != -1)
				grid[highlightIdx].setFillColor(squareNormal);
			if (idx != -1)
				grid[idx].setFillColor(squareMouseHighlight);
			highlightIdx = idx;
			needRedraw = true;
		}
	}
	else if (highlightIdx != -1)
	{
		grid[highlightIdx].setFillColor(squareNormal);
		highlightIdx = -1;
		needRedraw = true;
	}
}

void GUI::handleMouseClick(int const& mouseX, int const& mouseY)
{
	if (ev.mouseButton.button != sf::Mouse::Left
		|| highlightIdx == -1
		|| humanMoveIdx != -1
		|| gameEnded())
		return;
	playerMove(human, highlightIdx);
	humanMoveIdx = highlightIdx;
}

void GUI::handleMouseLeftWindow()
{
	if (highlightIdx != -1)
	{
		grid[highlightIdx].setFillColor(squareNormal);
		needRedraw = true;
	}
	highlightIdx = -1;
}

void GUI::drawX(sf::Vector2f const& pos)
{
	sf::RectangleShape line{ {1.5f * (SQUARE_DIM - 4 * SQUARE_MARGIN), SQUARE_MARGIN} };
	line.setOrigin(line.getSize().x * 0.5f, line.getSize().y * 0.5f);
	line.setPosition(pos.x + 0.5f * SQUARE_DIM, pos.y + 0.5f * SQUARE_DIM);
	line.setFillColor(colorX);
	line.rotate(45);
	window.draw(line);
	line.rotate(90);
	window.draw(line);
}

void GUI::drawO(sf::Vector2f const& pos)
{
	sf::CircleShape circle{ SQUARE_DIM * 0.5f - 2 * SQUARE_MARGIN };
	circle.setOutlineThickness(SQUARE_MARGIN);
	circle.setOutlineColor(colorO);
	circle.setFillColor(sf::Color::Transparent);
	circle.setPosition(pos.x + 2 * SQUARE_MARGIN, pos.y + 2 * SQUARE_MARGIN);
	window.draw(circle);
}

void GUI::setStatusText(std::string const& text)
{
	status.setString(text);
	float statusWidth{ status.getGlobalBounds().width };
	status.setPosition(0.5f * (gridDim - statusWidth - SQUARE_DIM), gridDim + SQUARE_MARGIN);
}

void GUI::drawStatus()
{
	static bool init = false;
	static sf::Vector2f drawPos;

	if (!init)
	{
		init = true;
		setStatusText("Current player : ");
		drawPos = {
			status.getGlobalBounds().left + status.getGlobalBounds().width + SQUARE_MARGIN,
			status.getGlobalBounds().top - 0.5f * (SQUARE_DIM - TEXT_SIZE + 6)
		};
	}
	else if (current == PLAYER_X)
		drawX(drawPos);
	else if (current == PLAYER_O)
		drawO(drawPos);
	else if (winner)
	{
		setStatusText("Winner : ");
		drawPos = {
			status.getGlobalBounds().left + status.getGlobalBounds().width + SQUARE_MARGIN,
			status.getGlobalBounds().top - 0.5f * (SQUARE_DIM - TEXT_SIZE + 6)
		};
		if (winner == PLAYER_X)
			drawX(drawPos);
		else
			drawO(drawPos);
	}
	else
		setStatusText("Game drawn !");
	window.draw(status);
}
