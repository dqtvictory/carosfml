#pragma once

#include <thread>
#include <SFML/Graphics.hpp>
#include "Common.h"

#define WIN_TITLE "Caro"
#define FONT_FNAME "Roboto.ttf"
#define SQUARE_DIM 30
#define SQUARE_MARGIN 3
#define TEXT_SIZE 20

class GUI
{
public:
	GUI(int firstPlayer, int humanPlayer, int& humanMoveIdx, int& cpuMoveIdx);
	void GUILoop();
	void HasWinner(int player);

private:
	const sf::Color windowBG = sf::Color::Black;
	const sf::Color squareNormal = sf::Color::White;
	const sf::Color squareMouseHighlight = sf::Color::Yellow;
	const sf::Color squareMoveHighlight = sf::Color::Green;
	const sf::Color colorX = sf::Color::Blue;
	const sf::Color colorO = sf::Color::Red;

	bool needRedraw;
	sf::RenderWindow window;
	sf::Font font;
	sf::Text status;
	sf::Event ev;
	array<sf::RectangleShape, BOARD_SIZE> grid;

	int const human, cpu;
	map<int, set<int> > plays;
	int highlightIdx, lastMoveIdx;
	int current, winner;
	int& humanMoveIdx, & cpuMoveIdx;

	bool gameEnded();
	void drawWindow();
	void playerMove(int player, int moveIdx);
	
	void handleMouseMove(int const& mouseX, int const& mouseY);
	void handleMouseClick(int const& mouseX, int const& mouseY);
	void handleMouseLeftWindow();
	
	void drawX(sf::Vector2f const& pos);
	void drawO(sf::Vector2f const& pos);
	void setStatusText(std::string const &text);
	void drawStatus();
};
