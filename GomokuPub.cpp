#include <iostream>
#include "Gomoku.h"

array<Vec2D, BOARD_SIZE> const& Gomoku::GetIdxTable()
{
	return idxTable;
}

Gomoku::Gomoku() :
	moveCount{ 0 },
	lastMoveIdx{ -1 },
	boardScore{ 0 },
	gravity{ -1, -1 },
	CurrentPlayer{ PLAYER_X },
	Winner{ 0 }
{
	gomokuInit();
	board[PLAYER_X].reserve(BOARD_SIZE);
	board[PLAYER_O].reserve(BOARD_SIZE);
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		board[PLAYER_X].push_back(false);
		board[PLAYER_O].push_back(false);
		if (0 <= i && i <= 5)
		{
			threats[PLAYER_X].push_back(set<Vec2D>());
			threats[PLAYER_O].push_back(set<Vec2D>());
		}
	}
	moveHistory.reserve(BOARD_SIZE);
	moveIdxHistory.reserve(BOARD_SIZE);
	gravityHistory.reserve(BOARD_SIZE);
}

bool Gomoku::IsEnd() const
{
	return Winner || (moveCount == BOARD_SIZE);
}

void Gomoku::Move(int moveIdx)
{
	board[CurrentPlayer][moveIdx] = true;
	updateScore(moveIdx);
	lastMoveIdx = moveIdx;
	++moveCount;
	updateGravity();
	CurrentPlayer *= -1;
}

void Gomoku::UndoMove(int moveIdx)
{
	CurrentPlayer *= -1;
	auto& threatsPl{ threats.at(CurrentPlayer) };
	auto& threatsOp{ threats.at(-CurrentPlayer) };
	int moveScore{ 0 };

	auto& history{ moveHistory.back() };
	for (auto it{ history.rbegin() }; it != history.rend(); ++it)
	{
		MoveHistory& hist{ *it };
		if (hist.moveType == TYPE_ADD)
		{
			threatsPl[hist.threatCount].erase(hist.threat);
			if (hist.threatCount != 1)
				threatsPl[hist.threatCount - 1].insert(hist.threat);
			if (hist.threatCount == 5)
				Winner = 0;
			moveScore += scoreTable[hist.threatCount] - scoreTable[hist.threatCount - 1];
		}
		else
		{
			moveScore += scoreTable[hist.threatCount];
			threatsNull.erase(hist.threat);
			threatsOp[hist.threatCount].insert(hist.threat);
		}
	}
	gravity = gravityHistory.back();
	lastMoveIdx = moveIdxHistory.back();
	moveHistory.pop_back();
	gravityHistory.pop_back();
	moveIdxHistory.pop_back();
	boardScore -= CurrentPlayer * moveScore;
	board[CurrentPlayer][moveIdx] = false;
	--moveCount;
}

int Gomoku::Eval() const
{
	return boardScore + CurrentPlayer * TURN_BONUS;
}

void Gomoku::GetPossibleMoves(vector<int>& moveQueue) const
{
	if (!moveCount)
		moveQueue.push_back(rcTable[{ BOARD_DIM / 2, BOARD_DIM / 2 }]);
	else if (moveCount == 1)
		secondMove(moveQueue);
	else if (moveCount < 5)
		openingMove(moveQueue);
	else
	{
		auto& threatsPl{ threats.at(CurrentPlayer) };
		auto& threatsOp{ threats.at(-CurrentPlayer) };
		if (threatsPl[4].size())
			handle4(moveQueue, threatsPl[4]);
		else if (threatsOp[4].size())
			handle4(moveQueue, threatsOp[4]);
		else if (threatsPl[3].size() >= 2)
			handle3(moveQueue, threatsPl[3]);
		else if (threatsOp[3].size() >= 2)
			handle3(moveQueue, threatsOp[3]);
		else
		{
			for (int i = 0; i < BOARD_SIZE; ++i)
				if (!board.at(PLAYER_X)[i] && !board.at(PLAYER_O)[i])
					moveQueue.push_back(i);
			std::sort(
				moveQueue.begin(),
				moveQueue.end(),
				[this](int i1, int i2)
				{
					auto p1{ GetIdxTable()[i1] };
					auto p2{ GetIdxTable()[i2] };
					return pointDistanceSquare(GetGravity(), p1) < pointDistanceSquare(GetGravity(), p2);
				}
			);
		}
	}
}

Pos2D const& Gomoku::GetGravity() const
{
	return gravity;
}
