# Gomoku Man vs Machine

This is a GUI version of the game Gomoku (5-in-a-row), where each player tries to connect 5 pawns in a row to win on a 16x16 board. The original game, originated from Japan, plays on a 19x19 board with black and white pawns, but I chose 16 for optimization reason. Plus, pawns in this version are cross (X) and circle (O), like the Vietnamese version that pupils there are used to playing on square-grid papers.

The AI is quite good at depth level 4, but not fast enough. This is due to numerous allocations and deallocations on the heap done by C++'s STL set, plus good move ordering are yet to be implemented. However, I believe by simply pre-allocate enough memory for the AI to work, the bot can think much much faster.

To compile the program, the command `make` suffice, provided SFML is already installed. If not, please follow the instructions on its website.

To run, simply execute `./gomoku` for default AI depth level 3, or `./gomoku 5` for AI depth level 5 for example.

Have fun!
