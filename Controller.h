#pragma once

#include <thread>
#include "GUI.h"
#include "Gomoku.h"

class Controller
{
public:
	Controller(int humanPlayer, int cpuLevel);
	void MainLoop();

private:
	int const human, cpuDepth;
	int humanMoveIdx, cpuMoveIdx;
	GUI gui;
	Gomoku game;

	void loop();
	int cpuPlay();
};
