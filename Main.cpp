#include <cstdlib>
#include <memory>
#include "Controller.h"

int main(int ac, char** av)
{
    int cpuLevel;
    if (ac == 1)
        cpuLevel = 3;
    else 
        cpuLevel = atoi(av[1]);
    if (cpuLevel <= 0)
        cpuLevel = 3;
    auto gameController{ std::make_unique<Controller>(PLAYER_X, cpuLevel) };
    gameController->MainLoop();
    return 0;
}