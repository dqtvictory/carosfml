#include <ctime>
#include "Gomoku.h"

static bool initialized = false;

array<Vec2D, BOARD_SIZE> Gomoku::idxTable;
map<Vec2D, int> Gomoku::rcTable;
map<int, int> Gomoku::scoreTable;

double Gomoku::pointDistanceSquare(Pos2D const& p1, Vec2D  const& p2)
{
	auto& [y1, x1] { p1 };
	auto& [y2, x2] { p2 };
	return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}

void Gomoku::gomokuInit()
{
	if (initialized)
		return;
	initialized = true;
	srand(time(nullptr));
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		Vec2D rc{ i / BOARD_DIM, i % BOARD_DIM };
		idxTable[i] = rc;
		rcTable[rc] = i;
	}
	scoreTable[0] = SCORE_0;
	scoreTable[1] = SCORE_1;
	scoreTable[2] = SCORE_2;
	scoreTable[3] = SCORE_3;
	scoreTable[4] = SCORE_4;
	scoreTable[5] = SCORE_5;
}

bool Gomoku::pointDistCompare(int p1, int p2)
{
	auto& [y1, x1] { idxTable[p1] };
	auto& [y2, x2] { idxTable[p2] };
	auto& [gy, gx] { gravity };

	double d1{ (y1 - gy) * (y1 - gy) + (x1 - gx) * (x1 - gx) };
	double d2{ (y2 - gy) * (y2 - gy) + (x2 - gx) * (x2 - gx) };
	return d1 < d2;
}

void Gomoku::updateScore(int moveIdx)
{
	moveHistory.push_back(vector<MoveHistory>());
	auto& hist{ moveHistory.back() };
	auto& threatsPl{ threats.at(CurrentPlayer) };
	auto& threatsOp{ threats.at(-CurrentPlayer) };
	auto& [r, c] { idxTable[moveIdx] };
	int moveScore{ 0 };
	for (auto& [dr, dc] : rcDiffs)
		for (int i = 4; i >= 0; --i)
		{
			int	rStart{ r - i * dr }, cStart{ c - i * dc };
			int rEnd{ rStart + 4 * dr }, cEnd{ cStart + 4 * dc };
			if (!rcLegal({ rStart, cStart }) || !rcLegal({ rEnd, cEnd }))
				continue;
			int idxStart{ rcTable[{ rStart, cStart }] }, idxEnd{ rcTable[{ rEnd, cEnd }] };
			Vec2D key{ std::min(idxStart, idxEnd), std::max(idxStart, idxEnd) };
			int count{ 1 };
			for (; count < 5; ++count)
			{
				if (threatsPl[count].find(key) != threatsPl[count].end())
				{
					threatsPl[count].erase(key);
					threatsPl[count + 1].insert(key);
					hist.push_back(MoveHistory{ key, count + 1, TYPE_ADD });
					moveScore += scoreTable[count + 1] - scoreTable[count];
					if (count == 4)
					{
						Winner = CurrentPlayer;
						goto func_return;
					}
					break;
				}
				else if (threatsOp[count].find(key) != threatsOp[count].end())
				{
					threatsOp[count].erase(key);
					threatsNull.insert(key);
					hist.push_back(MoveHistory{ key, count, TYPE_ELIM });
					moveScore += scoreTable[count];
					break;
				}
			}
			if (count == 5 && threatsNull.find(key) == threatsNull.end())
			{
				threatsPl[1].insert(key);
				moveScore += scoreTable[1];
				hist.push_back(MoveHistory{ key, 1, TYPE_ADD });
			}
		}
func_return:
	moveIdxHistory.push_back(lastMoveIdx);
	boardScore += CurrentPlayer * moveScore;
}

void Gomoku::updateGravity()
{
	gravityHistory.push_back(gravity);
	auto& [y, x] { idxTable[lastMoveIdx] };
	if (!moveCount)
		gravity = { y, x };
	else
	{
		auto& [gy, gx] { gravity };
		gravity =
		{
			(gy * (moveCount - 1) + y) / moveCount,
			(gx * (moveCount - 1) + x) / moveCount
		};
	}
}

bool Gomoku::rcLegal(Vec2D rc) const
{
	auto& [r, c] { rc };
	return r >= 0 && r < BOARD_DIM&& c >= 0 && c < BOARD_DIM;
}

void Gomoku::handle3(vector<int>& moveQueue, set<Vec2D> const& threats3) const
{
	set<int> priority;
	for (auto& [idxStart, idxEnd] : threats3)
		for (int i = idxStart; i <= idxEnd; i += (idxEnd - idxStart) / 4)
			if (!board.at(PLAYER_X)[i] && !board.at(PLAYER_O)[i])
				priority.insert(i);
	moveQueue.insert(moveQueue.begin(), priority.begin(), priority.end());
	for (int i = 0; i < BOARD_SIZE; ++i)
		if (!board.at(PLAYER_X)[i] && !board.at(PLAYER_O)[i] && priority.find(i) == priority.end())
			moveQueue.push_back(i);
	std::sort(
		moveQueue.begin() + priority.size(),
		moveQueue.end(),
		[this](int i1, int i2)
		{
			auto p1{ GetIdxTable()[i1] };
			auto p2{ GetIdxTable()[i2] };
			return pointDistanceSquare(GetGravity(), p1) < pointDistanceSquare(GetGravity(), p2);
		}
	);
}

void Gomoku::handle4(vector<int>& moveQueue, set<Vec2D> const& threats4) const
{
	auto& [idxStart, idxEnd] { *threats4.begin() };
	for (int i = idxStart; i <= idxEnd; i += (idxEnd - idxStart) / 4)
		if (!board.at(PLAYER_X)[i] && !board.at(PLAYER_O)[i])
		{
			moveQueue.push_back(i);
			return;
		}
}

void Gomoku::secondMove(vector<int>& moveQueue) const
{
	auto& [r, c] { idxTable[lastMoveIdx] };
	if (r < 2 || r >= BOARD_DIM - 2 || c < 2 || c >= BOARD_DIM - 2)
		moveQueue.push_back(rcTable[{ BOARD_DIM / 2, BOARD_DIM / 2 }]);
	else
	{
		auto& [dr, dc] { neighborDiffs[rand() % 8] };
		moveQueue.push_back(rcTable[{ r + dr, c + dc }]);
	}
}

void Gomoku::openingMove(vector<int>& moveQueue) const
{
	set<int> emptyNeighbors;
	for (int i = 0; i < BOARD_SIZE; ++i)
		if (board.at(CurrentPlayer)[i])
		{
			auto& [r, c] { idxTable[i] };
			for (auto& [dr, dc] : neighborDiffs)
				if (rcLegal({ r + dr, c + dc }))
				{
					int idxNeigh{ rcTable[{ r + dr, c + dc }] };
					if (!board.at(CurrentPlayer)[idxNeigh] && !board.at(-CurrentPlayer)[idxNeigh])
						emptyNeighbors.insert(idxNeigh);
				}
		}
	moveQueue.insert(moveQueue.begin(), emptyNeighbors.begin(), emptyNeighbors.end());
}
