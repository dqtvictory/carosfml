INC_DIR		= .

SRCS		=	Controller.cpp	GUI.cpp	GomokuPrv.cpp	GomokuPub.cpp	Main.cpp

OBJS		= $(SRCS:.cpp=.o)

NAME		= gomoku

CC			= g++
FLAGS		= -O3 -std=c++17 -lsfml-graphics -lsfml-system -lsfml-window

RM			= rm -f

.cpp.o:
			@$(CC) $(FLAGS) -I$(INC_DIR) -c $< -o $(<:.cpp=.o)

$(NAME):	$(OBJS)
			@echo "Object files compiled"
			@$(CC) $(FLAGS) $(OBJS) -I$(INC_DIR) -o $(NAME)
			@echo "Executable created"
			@echo "Compilation finished"

all:		$(NAME)

clean:
			@$(RM) $(OBJS)
			@echo "Deleted all but executable"

fclean:		clean
			@$(RM) $(NAME)
			@echo "Everything deleted"

re:			fclean all

.PHONY:		all clean fclean re
