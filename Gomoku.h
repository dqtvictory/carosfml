#pragma once

#include "Common.h"

#define SCORE_0		0
#define SCORE_1		1
#define SCORE_2		8
#define SCORE_3		64
#define SCORE_4		512
#define SCORE_5		16777216
#define TURN_BONUS	4

#define TYPE_ADD	0
#define TYPE_ELIM	1

struct MoveHistory
{
	Vec2D threat;
	int threatCount;
	int moveType;
};

class Gomoku
{
private:
	const array<Vec2D, 4> rcDiffs
	{ {
		{1, 0},	// N-S
		{0, 1},	// W-E
		{1, 1},	// NW-SE
		{1, -1}	// NE-SW
	} };
	const array<Vec2D, 8> neighborDiffs
	{ {
		{0, 1}, {1, 0}, {0, -1}, {-1, 0},
		{1, 1}, {1, -1}, {-1, -1}, {-1, 1}
	} };

	static array<Vec2D, BOARD_SIZE> idxTable;
	static map<Vec2D, int> rcTable;
	static map<int, int> scoreTable;

	map<int, vector<set<Vec2D> > > threats;
	set<Vec2D > threatsNull;
	vector<vector<MoveHistory> > moveHistory;
	vector<int> moveIdxHistory;
	vector<Pos2D > gravityHistory;

	map<int, vector<bool> > board;
	int moveCount;
	int lastMoveIdx;
	int boardScore;
	Pos2D gravity;

public:
	int CurrentPlayer;
	int Winner;

	static array<Vec2D, BOARD_SIZE> const& GetIdxTable();
	Gomoku();
	Gomoku(Gomoku const& src) = default;
	void Print();
	bool IsEnd() const;
	void Move(int moveIdx);
	void UndoMove(int moveIdx);
	int Eval() const;
	void GetPossibleMoves(vector<int>& moveQueue) const;
	Pos2D const& GetGravity() const;

private:
	static void gomokuInit();
	static double pointDistanceSquare(Pos2D const& p1, Vec2D  const& p2);
	bool pointDistCompare(int p1, int p2);
	void updateScore(int moveIdx);
	void updateGravity();
	bool rcLegal(Vec2D rc) const;
	void handle3(vector<int>& moveQueue, set<Vec2D> const& threats3) const;
	void handle4(vector<int>& moveQueue, set<Vec2D> const& threats4) const;
	void secondMove(vector<int>& moveQueue) const;
	void openingMove(vector<int>& moveQueue) const;

};
