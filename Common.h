#pragma once

#include <array>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <cstdlib>

#define BOARD_DIM	16
#define BOARD_SIZE	256

#define PLAYER_X	1
#define PLAYER_O	-1

using std::array;
using std::map;
using std::set;
using std::vector;

typedef std::pair<int, int> Vec2D;
typedef std::pair<double, double> Pos2D;
